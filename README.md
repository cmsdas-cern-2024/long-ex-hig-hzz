# long-ex-hig-hzz

## Abstract
This comprehensive exercise aims to measure the cross section of the Standard Model Higgs Boson (H) in the H->ZZ->4l final state.Advanced techniques for event selection will be implemented to reconstruct the H mass. Signal and background estimation will be employed to quantify the production rate accurately. Additionally, we will also introduce combine tool which will ensure robust statistical analysis. Overall this exercise offers to the students a broad overview of the various steps involved in a standard analysis in CMS experiment.

## Getting started
Link to the presentation: 
    [link](https://docs.google.com/presentation/d/1aHQxfqPqUSMB5llgBgZ6H_YjOwtF7wDx-26JVuQfyKI/edit#slide=id.g2e54e2d6285_0_26)

Main EOS directory is: 
    /eos/user/c/cmsdas/2024/long-ex-hig-hzz/

Istructions to set and run the code are here: /eos/user/c/cmsdas/2024/long-ex-hig-hzz/HowToRun.txt

## Facilitators:
- Antra Gaile [Antra.Gaile@cern.ch]
- Filippo Errico [Filippo.errico@cern.ch]
- Ilirjan Margjeka [Ilirjan.Margjeka@cern.ch]
- Sadhana Verma [Sadhana.Verma@cern.ch]

## Exercise
The relevant instruction could be found in the file:
```
/eos/user/c/cmsdas/2024/long-ex-hig-hzz/HowToRun.txt [file n.1]
```

### Step1: reconstruction of the Higgs boson
In this step of the analysis, objects need to be reconstructed.
Conditions on the objects are defined here:
```
/eos/user/c/cmsdas/2024/long-ex-hig-hzz/LeptonSelectorCppWorker.cc
```
[Open the file and check which if all the conditions needed have been]

Once defined all object conditions, run the code following instruction in file n.1 and check few important distributions.
[Open the ROOT file just produced to check which variables are stored and how you can select good events.]

In case you need, here the reference to nanoAOD (v12) format:
[link](https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv12/2022/2023/doc_DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8_Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2.html#GenVisTau)

### Step2: Increase the statistics
Using Run 2 samples provided in the /eos/ directory, plot invariant mass distributions for signal and background using 
```
/eos/user/c/cmsdas/2024/long-ex-hig-hzz/ExampleDraw.C
```

### Step3: Signal vs background shape
Once obtained 4lepton invariant mass distribution, the model to extract our final result (e.g. signal strengh or the mass) needs to be built. First of all signal and background shape are needed.
Using samples in "Samples" directory (see file n.1), fit the 4l mass distribution for both signal and background.
A reference model on how to implement the fit of an histogram, could be found here
```
eos/user/c/cmsdas/2024/long-ex-hig-hzz/ExampleSimpleFit.C
```

### Step4: Datacard 
Once all the ingredients (but left aside only the systematic uncertainties for the momemt) the datacard could be inspectioned.
Have a look at different datacard here:
```
/eos/user/c/cmsdas/2024/long-ex-hig-hzz/datacards/cards_V5_2018_withBkg_1D/
```

### Step5: Run the measurement
Using combine perform the signal strength and mass measurements